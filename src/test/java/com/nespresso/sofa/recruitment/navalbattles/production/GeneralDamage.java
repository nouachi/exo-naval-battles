package com.nespresso.sofa.recruitment.navalbattles.production;

import java.util.List;

/**
 * Created by user on 24/03/15.
 */
public class GeneralDamage implements GetDamageStrategy {
    @Override
    public void getAShoot(double damage, List<Ship> availableShips, List<Ship> sunksShips) {
        Ship currentAttackedShip = availableShips.get(0);
        currentAttackedShip.getAShoot(damage);
        if(currentAttackedShip.leftScore() < 0) {
            sunksShips.add(currentAttackedShip);
            availableShips.remove(0);
        }
    }
}
