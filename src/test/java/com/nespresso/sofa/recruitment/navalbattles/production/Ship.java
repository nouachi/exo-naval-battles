package com.nespresso.sofa.recruitment.navalbattles.production;

/**
 * Created by user on 24/03/15.
 */
public class Ship implements Comparable<Ship>, Attacker {
    private int score;
    private  int canons;
    protected int displacement;
    protected int masts;



    public Ship(int... displacementAndMatsAndCanon) {
        displacement = ShipParser.parseDisplacememt(displacementAndMatsAndCanon);
        masts = ShipParser.parseMats(displacementAndMatsAndCanon);
        canons = ShipParser.parseCanons(displacementAndMatsAndCanon);
        score = displacement + (100 * canons) + (1000 * masts);
    }

    public void fight(Attacker attacker) {
        while(this.stillAlive() && attacker.stillAlive()){
            attacker.attack(this);
            this.attack(attacker);
        }
    }

    public void attack(Attacker attacker) {
        attacker.getAShoot(this.getShootDamageCapacity());
    }

    @Override
    public void getAShoot(double damage) {
        score -= damage;
    }

    @Override
    public double getShootDamageCapacity() {
        return canons * 200;
    }

    @Override
    public boolean stillAlive() {
        return score > 0;
    }

    public int leftScore() {
        return score;
    }

    public int compareTo(Ship secondShip) {
        double firstShipSpeed = (displacement / masts ) * (1 + canons*0.005);
        double secondShipSpeed = (secondShip.displacement / secondShip.masts) * (1 + secondShip.canons*0.005);
        return Double.valueOf(firstShipSpeed).compareTo(Double.valueOf(secondShipSpeed));
    }

    public void destroyAnEquipment(double damage) {
        if(MastsStillThere()) {
            destroyMasts(damage);
        }
        else if( aCanonStillThere()){
            destroyCanons(damage);
        }
    }

    private void destroyCanons(double damage) {
        if(canons - damage > 0)
            canons = (int) Math.round(canons - damage);
        else
            canons = 0;
    }

    private void destroyMasts(double damage) {
        if(masts - damage > 0){
            masts = (int) Math.round(canons - damage);
        }else{
            masts = 0;
            destroyAnEquipment(damage - masts);
        }
    }

    private boolean aCanonStillThere() {
        return canons != 0;
    }

    private boolean MastsStillThere() {
        return masts != 0;
    }
}
