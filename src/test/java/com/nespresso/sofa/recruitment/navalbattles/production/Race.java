package com.nespresso.sofa.recruitment.navalbattles.production;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by user on 24/03/15.
 */
public class Race {

    private List<Ship> ships;

    public Race(Ship ... shipsArray) {
        ships = Arrays.asList(shipsArray);
    }

    public Ship winner() {
        Collections.sort(ships);
        return ships.get(0);
    }
}
