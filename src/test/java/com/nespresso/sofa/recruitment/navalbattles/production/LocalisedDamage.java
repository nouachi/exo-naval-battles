package com.nespresso.sofa.recruitment.navalbattles.production;

import java.util.List;

/**
 * Created by user on 24/03/15.
 */
public class LocalisedDamage implements GetDamageStrategy {

    @Override
    public void getAShoot(double damage, List<Ship> availableShips, List<Ship> sunksShips) {
        Ship currentShip = availableShips.get(0);
        currentShip.destroyAnEquipment(damage);
        currentShip.getAShoot(damage);
        if(currentShip.leftScore() < 0) {
            sunksShips.add(currentShip);
            availableShips.remove(0);
        }
    }
}
