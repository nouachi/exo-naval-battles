package com.nespresso.sofa.recruitment.navalbattles.production;

import java.util.List;

/**
 * Created by user on 24/03/15.
 */
public interface GetDamageStrategy {

    void getAShoot(double damage, List<Ship> availableShips, List<Ship> sunksShips);
}
