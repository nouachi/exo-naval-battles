package com.nespresso.sofa.recruitment.navalbattles.production;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 24/03/15.
 */
public class Team implements Attacker{

    private List<Ship> availableShips;
    private List<Ship> sunksShips = new ArrayList<>();

    private GetDamageStrategy damageStrategy;

    public Team(List<Ship> availableShips) {
        this.availableShips = availableShips;
    }

    @Override
    public void attack(Attacker attacker) {
        attacker.getAShoot(this.getShootDamageCapacity());

    }

    @Override
    public boolean stillAlive() {
        return availableShips.size() > 0;
    }

    @Override
    public void getAShoot(double damage) {
        Ship currentAttackedShip = availableShips.get(0);
        currentAttackedShip.getAShoot(damage);
        if(currentAttackedShip.leftScore() < 0) {
            sunksShips.add(currentAttackedShip);
            availableShips.remove(0);
        }
    }

    @Override
    public double getShootDamageCapacity() {
        double shootCapacity = 0;
        if(availableShips.size() > 1){
            for (Ship ship : availableShips) {
                shootCapacity += ship.getShootDamageCapacity() * 1.15;
            }
        }else{
            shootCapacity = availableShips.get(0).getShootDamageCapacity();
        }
        return shootCapacity;
    }

    public boolean contains(Ship ship) {
        return availableShips.contains(ship) || sunksShips.contains(ship);
    }

    public void setDamageStrategy(GetDamageStrategy damageStrategy) {
        this.damageStrategy = damageStrategy;
    }
}
