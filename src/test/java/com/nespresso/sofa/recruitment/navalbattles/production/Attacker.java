package com.nespresso.sofa.recruitment.navalbattles.production;

/**
 * Created by user on 24/03/15.
 */
public interface Attacker {

    public void attack(Attacker attacker);

    boolean stillAlive();

    void getAShoot(double damage);

    double getShootDamageCapacity();
}
