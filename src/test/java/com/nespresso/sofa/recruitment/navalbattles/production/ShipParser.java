package com.nespresso.sofa.recruitment.navalbattles.production;

/**
 * Created by user on 24/03/15.
 */
public class ShipParser {

    public static int parseCanons(int[] displacementAndMatsAndCanon) {
        if(displacementAndMatsAndCanon.length == 3)
            return displacementAndMatsAndCanon[2];
        return 0;
    }

    public static int parseMats(int[] displacementAndMatsAndCanon) {
        return displacementAndMatsAndCanon[1];
    }

    public static int parseDisplacememt(int[] displacementAndMatsAndCanon) {
        return displacementAndMatsAndCanon[0];
    }
}
