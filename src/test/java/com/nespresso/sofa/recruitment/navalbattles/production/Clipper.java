package com.nespresso.sofa.recruitment.navalbattles.production;

/**
 * Created by user on 24/03/15.
 */
public class Clipper extends Ship implements Comparable<Ship>{

    public static final double SPEED_BONUS = 0.2;

    public Clipper(int ... displacementAndMatsAndCanon) {
        super(displacementAndMatsAndCanon);
    }

    @Override
    public int compareTo(Ship secondShip) {
        double clipperSpeed = (displacement / masts) * (1 - SPEED_BONUS);
        double shipSpeed = secondShip.displacement / secondShip.masts;
        return Double.valueOf(clipperSpeed).compareTo(Double.valueOf(shipSpeed));
    }
}
