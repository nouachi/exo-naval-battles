package com.nespresso.sofa.recruitment.navalbattles.production;

import java.util.ArrayList;

import static java.util.Arrays.asList;

/**
 * Created by user on 24/03/15.
 */
public class Battle {

    public static String LOCALIZED_DAMAGES = "LOCALIZED_DAMAGES";


    private Ship firstSide;
    private Team team;
    private String damageType;

    public Battle() {
    }

    public Battle(String damageType) {
        this.damageType = damageType;
    }

    public boolean isInTheWinningSide(Ship ship) {
        if(isTheShipBelongToAWinnerTeam(ship) || ship.stillAlive())
            return true;
        return false;
    }

    public Battle side(Ship firstSide) {
        this.firstSide = firstSide;
        return this;
    }

    public Battle against(Ship ... ships) {
        Attacker attacker = getAttacker(ships);
        firstSide.fight(attacker);
        return this;
    }

    private Attacker getAttacker(Ship[] ships) {
        if(ships.length == 1)
            return ships[0];
        else {
            team = new Team(new ArrayList<>(asList(ships)));
            if(LOCALIZED_DAMAGES.equals(damageType))
                team.setDamageStrategy(new LocalisedDamage());
            else
                team.setDamageStrategy(new GeneralDamage());
            return team;
        }
    }

    private boolean isTheShipBelongToAWinnerTeam(Ship ship) {
        return team != null && team.contains(ship) && team.stillAlive();
    }
}
